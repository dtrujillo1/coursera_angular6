import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { VoteUpAction, VoteDownAction } from '../models/destinos-viajes-state.model';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {

  /**
   * Se utiliza el decorador Input para aclarar que la variable nombre puede ser pasada como parametro, es decir
   * inicializada desde algun otro lado
   */
  @Input()
  destino: DestinoViaje;

  /**
   * Lo que se hace a continuacion es que por medio de la etiqueta HostBinding se pueden asignar valores a propiedades o atributos
   * del tag que envuelve al componente actual. en este caso le estamos diciendo que al atributo "class" le asigne el valor de la
   * valriable cssClass, el cual es col-md-4. Esto seria igual a tener en el tag la propiedad class="col-md-4"
   */
  @HostBinding("attr.class")
  cssClass = 'col-md-4';

  /**
   * la variable clicked es una variable de tipo eventEmitter que me permite emitir un envento hacia mi componente padre. Esta variable
   * puede ser de tipo generico pero especificamos en este caso el objeto que manejaremos.
   * 
   * Esta variable tiene la etiqueta output por que sea una variable de salida
   */
  @Output()
  clicked: EventEmitter<DestinoViaje>;

  /**
   * Variable que contendra la posicion o index de la card de destino viaje presentada en pantalla. Aqui podemo ver que en la
   * etiqueta del input se le pasa un nombre, este nombre es como podra ser accedida a la variable position desde la vista, es
   * decir en el .ts sera llamada position y en la vista sera llamada idx.
   */
  @Input("idx")
  position: number;

  constructor(private store: Store<AppState>) {
    // inicializacion de la variable
    this.clicked = new EventEmitter();
   }

  ngOnInit(): void {
  }

  /**
   * Metodo que me permite capturar lo sucedido cuando el usuairo da clic en el boton de la pantalla
   */
  ir() {
    /**
     * Aqui estamos diciendo que cuando entre a este metodo utilizaremos la variable clicked para emitir un evento, en nuestro caso
     * emitir la variable de destino hacia nuestro componente padre
     */
    this.clicked.emit(this.destino);

    //recordar que retorno un false para que no me recargue la pagina.
    return false;
  }


  public voteUp() {
    //this.store.dispatch(new VoteUpAction(Object.assign({},this.destino)));
    this.destino.votes++;
    return false;
  }

  public voteDown() {
    //this.store.dispatch(new VoteDownAction(Object.assign({},this.destino)));
    this.destino.votes--;
    return false;
  }

}

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';
import { DestinosApiCliente } from '../models/destinos-api-cliente.model';
import { Store } from "@ngrx/store";
import { AppState } from '../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../models/destinos-viajes-state.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {

  allDestinos: DestinoViaje[];

   //@Output()
   //onIntemAdded: EventEmitter<DestinoViaje>;

  updates: string[];
  
  /**
   * Metodo constructor que permite inicializar las variables u objetos.
   * 
   * Notas que en este caso estamos utilizando DestinosApliCliente inyectado directamente en el constructor. Esto se puede
   * hacer ya que en el appModulo esta inyectado en la seccion de providers y esto permite manejar el mismo objeto para toda
   * la aplicacion
   * @param destinosApiCliente 
   * @param store objeto que me contiene el estado de la aplicacion y por el cual puedo realizas una subscripcion
   */
  constructor(public destinosApiCliente: DestinosApiCliente, private store: Store<AppState>) { 
    //this.onIntemAdded = new EventEmitter();
    this.updates = [];

    /**
     * Lo que hago en esta parte es que accedo al dato de mi interes, el cual es el objeto DestinoViaje guardado
     * en favorito en la estructura o estado de mi aplicacion, y una vez accedo a el lo que digo es que sera un
     * observable para que realice la funcion de actualizar o agregar a arreglo de updates el mensaje con el nombre
     * del objeto elegido como favorito.
     * 
     * Tener en cuenta que esta forma de hacerlo con estados, remplaza la forma en la que lo hicimos en el siguiente
     * bloque de codigo.
     */
    this.store.select(state => state.destinos.favorito).subscribe(destinoFav => {
      if(destinoFav != null)
      {
        this.updates.push("Se ha elegido a "+ destinoFav.nombre);
      }
    });

    /**
     * Esta parte queda remplazada por la seccion de codigo anterior ya que se implementa por medio de Redux:estados
     * 
     * Lo que hacemos aqui es subcribir una funcion al observable creado en destinoApiCliente. y la funcion
     * qye queremos que realice ese observable es la especificada aqui, es decir mostrar un mensaje con el nombre
     * del destino viaje que se ha elegido. Esto es posible por que al suscribirlo aqui cada vez que el current de
     * destinosApiCliente sufra un cambio entonces se ejecutar esto. y cada vez que es elegido un lugar como favorito
     * en esta pantalla, se llama al elegirfavorito de destinoApliCliente y estamos diciendo en destinoApliCliente que 
     * el curren(observable) se actualice con el valor elegido.
     */
    // this.destinosApiCliente.subscribeOnChange((d: DestinoViaje) => {
    //   if(d != null)
    //   {
    //     this.updates.push("Se ha elegido a "+ d.nombre);
    //   }
    // })


    store.select(state => state.destinos.items).subscribe(items => this.allDestinos = items);
  }


  ngOnInit(): void {
  }


  /**
   * Este metodo remplaza al metodo que esta comentado en la parte de abajo (guardar) ya que se reestructura la aplicacion
   * y se indica que este componente recibira el objeto de destino emitido desde otro componente llamado form-destino-viaje.
   * @param destino Objeto de tipo DestinoViaje con la informacion de un destino
   */
  agregado(destino: DestinoViaje) {
    this.destinosApiCliente.add(destino);
    //this.onIntemAdded.emit(destino);

    
  }

  /**
   * Este metodo es remplazado por el metodo de agregado.
   * 
   * Como esta funcion esta siendo vinculada a un evento de un boton tiene que retornar un valor, en este caso boolean, Tener en 
   * cuenta que si retorna true se recargara la pagina y si retorna false no se recargara. En este caso vamos a retornar false para
   * que no recargue la pagina ya que es un pagina SPA
   * @param nombre string que representa el valor ingresado por pantalla por el input nombre
   * @param url string que representa el valor ingresado por pantalla por el input de url
   */
  // guardar(nombre:string, url:string):boolean{
  //   this.destinos.push(new DestinoViaje(nombre,url));
  //   return false;
  // }

  /**
   * Funcion que permite seleccionar como elegido el destinoViaje
   * @param item objeto de tipo destionViaje
   */
  elegido(item:DestinoViaje){
    this.destinosApiCliente.eligirDestinoViajeFavorito(item);
    
    
  }

  getAllDestinos(): DestinoViaje[] {
    return this.allDestinos;
  }

  

}

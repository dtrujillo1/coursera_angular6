import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router' //importaciones que permite el ruteo entre componentes de mi app
import { FormsModule, ReactiveFormsModule} from '@angular/forms'
import { StoreModule as NgRxStoreModule, ActionReducerMap } from "@ngrx/store";
import { EffectsModule  } from "@ngrx/effects";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";

import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './destino-detalle/destino-detalle.component';
import { DestinosApiCliente } from './models/destinos-api-cliente.model';
import { FormDestinoViajeComponent } from './form-destino-viaje/form-destino-viaje.component';
import { DestinoViajesState, reducerDestinosViajes, initializeDestinosViajesState, DestinosViajesEffects } from "./models/destinos-viajes-state.model";


/**
 * Aqui se hace una constante que tiene las rutas de mi proyecto, la primera ruta hace refenrencia a cuando llega vacio entonces
 * que redireccione a home, la seguna es que cuando llega con home entonces redirecione al componente de listaDestinos y cuando
 * llega destino entonces redireccione a al componente destinoDetalle. Una vez declaradas las rutas se especifican en la seccion
 * de imports para que las relacione al proyecto o aplicacion
 * 
 * Si quisiera colocar mas rutas pues aqui se especificarian
 */
const routes: Routes = [
  {path: '',redirectTo: 'home',pathMatch: 'full' },
  {path: 'home', component: ListaDestinosComponent},
  {path: 'destino', component: DestinoDetalleComponent}
];

/**
 * Redux init:  Aqui conectamos lo que declaramos en el archivo detinos-viajes-state.model.ts con la aplicacion
 *              para ser utilizado dentro de ella.
 */

/**
 * Se declara una interface que contien los estados de la aplicacion y en este caso solo tenemos un variable destinos
 * de tipo DestinoViajesState que controla el estado del destinoViaje
 */
export interface AppState {
  destinos: DestinoViajesState;
}

/**
 * Se hace una variable que contiene los reducers
 */
const reducers: ActionReducerMap<AppState> = {
  destinos: reducerDestinosViajes
};

/**
 * Se hace una variable que contiene la inicializacion de los estados
 */
const reducersInitialState = {
  destinos: initializeDestinosViajesState()
}
//redux fin init

@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),  //relaciono al proyecto las rutas declaradas anteriormente
    NgRxStoreModule.forRoot(reducers, { initialState: reducersInitialState }),  // se asigna a la aplicacion los reducers y estado inicial
    EffectsModule.forRoot([DestinosViajesEffects]),  // se agrega a la plicacion los effects
    StoreDevtoolsModule.instrument()  // Permite guardar los estados y verlos con las herramientas de desarrollador de google
  ],
  providers: [
    DestinosApiCliente
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map,filter,debounceTime,distinctUntilChanged,switchMap } from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';


@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {

  /**
   * Declaramos este objeto para que nos permite emitir el objeto guardado en el formulario hacia el componente de arriba
   * para que este pueda renderizarlo en pantalla segun su comportamiento interno.
   */
  @Output()
  onItemAdded: EventEmitter<DestinoViaje>;

  /**
   * Declaramos la variable de tipo FromGroup que esta asociada al formulario en la vista
   */
  fg: FormGroup;

  minLongitud: number = 5;

  /**
   * En esta variable se guardara la busqueda correspondiente al valor ingresado en el input de nombre, este arreglo
   * se inicializa en el metodo ngOnInit
   */
  busquedaResultados: string[];

  constructor(fb: FormBuilder) { 
    //inicializamos el objeto que nos permite emitir.
    this.onItemAdded = new EventEmitter();

    /**
     * Lo inicializamos por mdio del FormBuilder inyectandolo, ya que el FormBuilder permite la construccion de un FormGroup
     * dependiendo de la estrctura que le pasemos o indiquemos, en este caso la que va entre llaves, vemos una estructura de nombre
     * y url la cual es la que estamos utilizando en la vista. e inicialente iran vacios.
     * 
     * este tipo de formularios tambien nos permite agregar validaciones, como campos requerido, incluso crear nuestras
     * propias validaciones. Estas validaciones se veran reflejadas a nivel de formulario ya que si todos los campos
     * sn validos segun nuestras validaciones propuestas, entonces el formulario sera valida, de lo contrario sera no valido.
     * 
     * Observar que si queremos mas de una validacion a un control, lo que hacemos es llamas a Validator.Compose el cual nos 
     * permite ingresar un arreglo de validadores, en caso de que solo se quiera un validador se quita el Validator.Compose y se
     * coloca la validadcon.
     */
    this.fg = fb.group({
      nombre: ['', Validators.compose([Validators.required, this.nombreInvalidoValidator, this.nombreLongitudInvalidaValidatorParametrizable(this.minLongitud)])],
      url: ['']
    });

    this.fg.valueChanges.subscribe((form: any) => {
      console.log('cambio el formulario:' + form)
    });
  }

  /**
   * Este metodo es un metodo que se llama inmediatamente despues del contructor, la diferencia del constructor y este metodo
   * es que cuando se esta en el metodo de contructor, precisamente se esta contruyendo una instancia del tipo de clase, por lo
   * cual si esta en medio de la construccion no se puede hacer uso de ella, pero en el ngOnInit esta instancia ya fue finalizada
   * y se puede utilizar, entonces lo que se hace en el constructor es inicializar recursos que necesita la instancia que estamos
   * construyendo o creando y en el metodo ngOnInit se puede asignarle a la instancia tareas u ordenes que debe hacer la instancia
   * ya creada, como observables o demas.
   */
  ngOnInit(): void {
    // obtenemos el elmento del html con el id de nombre. Este elemento es un input
    let elementoNombre = <HTMLInputElement>document.getElementById("nombre");

    /**
     * Creamos un observable para el elemento obtenido y lo que hacemos es generarle un pipe, Esto es un flujo de acciones
     * que se iran ejecutando segun las condiciones que nosotros pongamos, En este caso lo que hacemos es:
     *    -mapeamos el valor ingresado en el input lo pasamos al siguiente
     *    -filtramos el valor que se pasa desde el pipe anterior y preguntamos que si ese texto tiene una longitud mayor o igual a dos para seguir.
     *    -lo que hacemos es una espera de 200ms como para esperar que ingresen mas letras
     *    -decimos que si lo que viene es diferente a lo que ya esta seguimos el flujo, de resto no seguiemos
     *    -en este punto lo que hacemos es obtener los valores por medio de ajax desde un json creado, este simula como una respuesta
     *    a una peticion donde nos devuelva los posibles nombres relacionado con lo que ya hemos ingresado. Aqui lo ideal es hace un llamado
     *    a una api que nos devuelva este resultado.
     * Una vez acamos el pipe lo que hacemos es suscribirse y lo que hacemos es que dentro de la suscripcion filtramos
     * la respuesta para que sea algo relacionado con lo que se esta escribiendo ya que en el json hay varios resultados
     */
    fromEvent(elementoNombre, 'input').pipe(
      map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
      filter(text => text.length >= 2),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(() => ajax('/assets/datos.json'))
    ).subscribe(ajaxResponse => {
      this.busquedaResultados = ajaxResponse.response;
      this.busquedaResultados = this.busquedaResultados.filter(x => x.toLowerCase().includes(elementoNombre.value.toString().toLowerCase()));
    });
  }

  /**
   * Aqui vemos como creamos el objeto DestinoViaje con la informacion pasada desde el formulario, y lo que hacemos una vez
   * construido es emitirlo por medio del objeto onItemAdded hacia el componente que encamsula este componente.
   * @param nombre
   * @param url 
   */
  guardar(nombre:string, url:string) :boolean {
    //construimos un objeto de tipo DestinoViaje con la inforamcion del formulario
    const destino = new DestinoViaje(nombre,url);

    //emitimos el destino que acabamos de construir con la informacion del formulario hacia el componente de arriba el cual es listaDestinos
    this.onItemAdded.emit(destino);

    
    /**
    * recordadr que se retorna un boolean ya que este esta siendo llamado en el submit del formulario. en este
    *  caso es false para que no recargue la pagina    
    */ 
    return false;
  }

  /**
   * Metodo que nos permite validar si el nombre es invalido o no de acuerdo a que si tiene o no una mayuscula. Es valido cuando
   * tiene por lo menos una mayuscula
   * @param control objeto de tipo FormControl al cual se le va a realizar la validacion. Observar que cuando se llama
   * en el arreglo de validaciones en el FormBuilder no se le pasa como parametro ya que por esta en ese control ya sabe
   * por defecto cual control sera el que tendra en cuenta.
   */
  nombreInvalidoValidator(control: FormControl): {[key: string]: boolean} {
    let nombre = control.value.toString().trim();
    let mayuscula = false;
    for (let index = 0; index < nombre.length; index++) {
      if(nombre.charAt(index) >= 'A' && nombre.charAt(index) <= 'Z')
      {
        mayuscula = true;
      }      
    }

    if(!mayuscula)
    {
      return {'invalidNombre': true};
    }
    
    return null;
  }

  /**
   * Metodo parametrizable que permite validar la longitu minima del nombre. esta longitud se pasa como parametro.
   * Esta seria la forma de hacer un validador al cual necesitemos pasarle un parametro para realizar la validacion.
   * @param minLong number que determina la longitud minima.
   */
  nombreLongitudInvalidaValidatorParametrizable(minLong: number): ValidatorFn{
    return (control: FormControl): {[key:string]: boolean} | null => {
      let longitud = control.value.toString().trim().length;
      if(longitud < minLong)
      {
        return {'minLongNombre': true};
      }
      return null;
    }
  }
}

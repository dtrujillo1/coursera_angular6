import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';
import { DestinosApiCliente } from '../models/destinos-api-cliente.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css']
})
export class DestinoDetalleComponent implements OnInit {
  destino: DestinoViaje;
  constructor(private route: ActivatedRoute, private destinosApiCliente: DestinosApiCliente) { }

  ngOnInit(): void {    
    const id = this.route.snapshot.paramMap.get('id');
    //this.destino = this.destinosApiCliente.getById(id);
  }

}

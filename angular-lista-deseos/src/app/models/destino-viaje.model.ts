import { VirtualTimeScheduler } from 'rxjs';
import { v4 as uuid} from 'uuid'
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';

/**
 * Clase que encapsula en comportamiento de un DestinoViaje
 * 
 * Tener en cuenta que sel pone la palabra reservada export cuando se quiere exportar esta clase fuera
 * del modulo en el que se encuentra creada.
 */
export class DestinoViaje{
    
    
    public selected: boolean;

    public servicios: string[];

    public id = uuid();

    public votes: number;

    /**
     * Metodo constructor de la clase DestinoViaje
     * 
     * Notese que se puede ahorrar la declaracion de las variables o atributos y el seteo de 
     * los valores dentro del constructor si lo hacemos declarando las variables en el
     * propio constructor.
     * @param nombre string que representa el nombre
     * @param url  string que representa la url de la imagen
     */
    constructor(public nombre: string, public url: string)
    {
        this.servicios = ['pileta','desayuno'];

        this.votes = 0;
    }

    public isSelected():boolean{
        return this.selected;
    }

    public setSelected(s:boolean){
        this.selected = s;
    }

    public getSelected(){
        return this.selected;
    }

    public getServicios():string[]{
        return this.servicios;
    }



    /*Esta es la forma tradicional como se haria la declaracion de variables y seteo de 
    valores por medio del constructor. pero en la parte de arriba se muestra una forma 
    mas sencilla de realizar lo mismo.

    nombre: string;
    url: string;
    
    constructor(nombre: string, url: string)
    {
        
        this.nombre = nombre;
        this.url = url;
        
    }
    */

    public voteUp() {
        this.votes++;
    }

    public voteDown() {
        this.votes--;
    }
}

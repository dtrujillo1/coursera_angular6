// Las siguiente importaciones se puede hacer gracias a a la instalacion del siguiente paquete "npm install --save @ngrx/store @ngrx/effects"

import { Injectable } from "@angular/core";
import { Action } from "@ngrx/store";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { DestinoViaje } from "./destino-viaje.model";

//---------------------------------------------------- ESTADO ---------------------------------------------------------

/**
 * Interface que me permite tener la estructura general del estado de mi aplicacion, en este caso es el estado de un
 * DestinoViaje favorito
 */
export interface DestinoViajesState {
    items: DestinoViaje[];  // Destinos que nos interezan 
    loading: boolean;       // Variable que nos permite saber si se haciendo el proceso de cargar o no.
    favorito: DestinoViaje; // DestinoViaje seleccionado como favorito
}

/**
 * Funcion que me permite inicializa el estado
 */
export function initializeDestinosViajesState() {
    return {
        items: [],
        loading: false,
        favorito: null
    }
}

//---------------------------------------------------- ACCIONES: Modifican el Estado ----------------------------------

/**
 * Clase que me agrupa los estados posibles de mi aplicacion
 */
export enum DestinosViajesActionTypes {
    NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
    ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
    VOTE_UP = '[Destinos Viajes] Vote Up',
    VOTE_DOWN = '[Destinos Viajes] Vote DOWN'
}

/**
 * Clase que me permite modificar el estado. En esta caso
 * es la creacion de nuevo DestinoViaje
 * 
 * Ver que son clases que implementan la interface Action 
 * la cual me permite utilizar la variable Type que es un
 * string.
 */
export class NuevoDestinoAction implements Action {
    type = DestinosViajesActionTypes.NUEVO_DESTINO;
    constructor(public destino: DestinoViaje) {}
}

/**
 * Clase que me permite modificar el estado. En esta caso
 * es la seleccion de un DestinoViaje como favorito 
 */
export class ElegidoFavoritoAction implements Action {
    type = DestinosViajesActionTypes.ELEGIDO_FAVORITO;
    constructor(public destino: DestinoViaje) {}
}

export class VoteUpAction implements Action {
    type = DestinosViajesActionTypes.VOTE_UP;
    constructor(public destino: DestinoViaje) {}
}

export class VoteDownAction implements Action {
    type = DestinosViajesActionTypes.VOTE_DOWN;
    constructor(public destino: DestinoViaje) {}
}

/**
 * Permite agrupar todos los tipos de datos de las aciones
 */
export type DestinosViajesActions = NuevoDestinoAction | ElegidoFavoritoAction | VoteUpAction | VoteDownAction;

//----------------------------------- REDUCERS(reductores): son llamados despues de cada accion -------------------------

//Los reducers son los encargados de la modificacion o mutacion de los estados.


/**
 * Este reducer se encarga de hacer la modificacion del estado despues de cada accion. Este Reductor lo que hace
 * es dependiendo de la accion toma una decision. Si la accion fue:
 *      -NUEVO_DESTINO: entonces lo que hace es agregar a items el DestinoViaje de interes y retornar
 *      -ELEGIDO_FAVORITO: entonces lo que hace es recorrer la lista de destino deseleccionando todos
 *      los destinos marcados como favoritos y obtiene de la accion el DestinoViaje seleccionado para
 *      marcarlo como favorito y retornarlo
 *      -Si no cumple con ningun caso entonces lo que hace es retornar el mismo estado que ingreso
 * 
 * Tener en cuenta que todos los reducers son llamados depues de una accion en el orden en que son codificados.
 * @param state estado inmediatamente antrior a la accion
 * @param action el tipo de accion que fue accionado
 */
export function reducerDestinosViajes (
    state: DestinoViajesState,
    action: DestinosViajesActions
): DestinoViajesState {    
    switch (action.type) {
        case DestinosViajesActionTypes.NUEVO_DESTINO: {
            return {
                ...state,
                items: [...state.items, (action as NuevoDestinoAction).destino]
            };
        }
        case DestinosViajesActionTypes.ELEGIDO_FAVORITO: {                        
            // state.items.forEach(x => x.setSelected(false));
            // let fav: DestinoViaje = (action as ElegidoFavoritoAction).destino;
            // fav.setSelected(true);            
            // return {
            //     ...state,
            //     favorito: fav
            // };

            //-----------------------------------

            return Object.assign({}, state, {
                items: state.items.map(aux => {

                    if(aux.nombre === (action as ElegidoFavoritoAction).destino.nombre)
                       {
                            return Object.assign({}, aux, {
                                selected: true
                             })
                           
                       }
                       else {
                            return Object.assign({}, aux, {
                                selected: false
                            })
                       }
                   
                }),
                favorito: Object.assign({}, (action as ElegidoFavoritoAction).destino, {                    
                    selected: true
                })
            });
        }
        case DestinosViajesActionTypes.VOTE_UP: {
            const d: DestinoViaje = (action as VoteUpAction).destino;
            d.votes++;
            return {...state}
        }
        case DestinosViajesActionTypes.VOTE_DOWN: {
            const d: DestinoViaje = (action as VoteDownAction).destino;
            d.votes--;
            return {...state}
        }
    }
    return state;
}


//----------------------------------- EFFECTS: son llamados despues de los Reducers  -------------------------

/**
 * Los effects permite que dependiendo de una accion sea llamado otra accion, en este caso es cuando la 
 * accion NUEVO_DESTINO se ejecuta, entonces se ejecutara la accion ELEGIDO_FAVORITO sobre el DestionViaje
 * que se agrego.
 */



@Injectable()
export class DestinosViajesEffects {
    @Effect()
    nuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(DestinosViajesActionTypes.NUEVO_DESTINO),
        map((action:NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
    );

    constructor(private actions$: Actions){}
}
    
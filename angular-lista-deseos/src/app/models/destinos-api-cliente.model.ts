import { DestinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject } from 'rxjs';
import { Store } from "@ngrx/store";
import { AppState } from '../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './destinos-viajes-state.model';
import { Injectable } from '@angular/core';

@Injectable()
export class DestinosApiCliente{
    private destinos: DestinoViaje[];

    /**
     * Observable de tipo BehaviosSubject. se especifica que valor por defecto en la inicializacion
     */
    current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);

    constructor(private store: Store<AppState>){
        /**
         * Ya no se utiliza este destinos porque todo se guarda en el store
         */

        // this.destinos = []; // se inicializa vacio el arreglo
    }

    add(destino:DestinoViaje) {
        // this.destinos.push(destino);

        // this.destinos.forEach(x => x.setSelected(false));
        // destino.setSelected(true);

        var copy = Object.assign({},destino);
        // Aqui se dispara una accion la cual es la de un nuevoDestino.
        this.store.dispatch(new NuevoDestinoAction(copy));
    }

    getAll():DestinoViaje[]{
        return this.destinos;
    }

    getById(id:string): DestinoViaje {
        return this.destinos.filter(function(d) {return d.id.toString() === id;})[0];
    }

    /**
     * Permite marcar el DestinoViaje favorito y asignarselo al observable current
     * @param destino Objeto de tipo Destino viaje que se selecciono como favorito
     */
    eligirDestinoViajeFavorito(destino: DestinoViaje) {
        // this.destinos.forEach(x => x.setSelected(false));
        // destino.setSelected(true);
        /**
         * Lo que hago aqui es pasarle el destino que fue selecciona como favorito al observable para que todos los
         * subscriptores a este sepan cual es el DestinoViaje favorito.
         */
        //this.current.next(destino);
        var copy = Object.assign({},destino);
        // Aqui se dispara una accion la cual es la de un Destino elegido como favorito.
        this.store.dispatch(new ElegidoFavoritoAction(copy));

        

        
       
    }

    /**
     * Este metodo se utilizaba cuando se hacia con observables, se sustituye ya que ahora se implemento
     * todo lo de Redux: estados
     * 
     * Metodo que permite subcribir una funcion al observable para que tengan acceso a sus cambios
     * @param fn funcion que se quiere suscribir al observable
     */
    // subscribeOnChange(fn: any) {
    //     this.current.subscribe(fn);
    // }


}
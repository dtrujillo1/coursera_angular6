import { Component } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-lista-deseos';

  /**
   * Se especifica que time sera un observable y se especifica de una ves un subscriptor llamado observer, ese observer
   * lo que va a ejecutar es la funcion de setInterval, y la ejecutara cada 1000mS es decir cada segundo, y lo que va a
   * ejecutar es la creacion de un nuevo objeto tipo Date con las fecha y hora del sistema y lo convertira en un string
   * para ser visualizado en pantalla por medio de la variable time. Es decir que la variable time es un observable de string.
   */
  time = new Observable(observer => {
    setInterval(() => observer.next(new Date().toString()) , 1000);
  });  
}
